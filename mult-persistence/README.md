# Multiplicative persistence

A nice experiment with Python about [Multiplicative Persistence](http://mathpickle.com/project/multiplicative-persistence/).

The idea for this little script came from [this video](https://www.youtube.com/watch?v=Wim9WJeDTHQ&t=762s) from [Numberphile](https://www.youtube.com/channel/UCoxcjq-8xIDTYp3uz647V5A) 

from itertools import count
import numpy as np


# Returns the list of digits of the passed number
def d_list(number):
    return [int(x) for x in str(number)]


# Recursive version, returns the
# Multiplicative persitence of the list
def persist_rec(digits, step):
    if (digits.size == 1):
        return step
    else:
        prod = np.multiply.reduce(digits)

        l_digits = np.array(d_list(prod))

        return persist_rec(l_digits, step+1)


# Multiplicative persistence of number
def mult_persist(number):
    digit_list = np.array(d_list(number))

    return persist_rec(digit_list, 0)


def main():

    maximo = 0
    print('maximo actual : 0')
    """
    It'l start from 0 and try with every number,
    Printing the new maximum found
    """
    for c in count(start=0):
        p = mult_persist(c)
        if p > maximo:
            print('Encontrado nuevo')
            print(c, p)
            maximo = p


if __name__ == '__main__':
    main()
